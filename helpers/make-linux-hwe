#!/bin/sh
#
#    Copyright (C) 2008-2018  Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=3

. ./config

for PATCH in $DATA/*.patch ; do
  echo $PATCH
  patch -p1 < $PATCH
done

rm -rf /tmp/preserve
mkdir /tmp/preserve
PRESERVE=$(grep '^+++' $DATA/silent-accept-firmware.patch | /bin/sed 's/+++ //; s/\t.*//;' | cut -d/ -f2- | sort -u )
for FILE in $PRESERVE; do
  cp $FILE /tmp/preserve --parents -a
done

sh $DATA/deblob-4.15 --force

echo "Reverting deblobbing for files patched by silent-accept-firmware"
cp /tmp/preserve/* . -av

# Remove ZFS
rm zfs spl debian/scripts/misc/update-zfs.sh -rf
/bin/sed 's/spl-dkms, zfs-dkms//' -i debian/control debian.master/control.d/vars.generic debian.master/control.d/vars.*
/bin/sed '/^define build_zfs/,/^endef/d; /^define install_zfs/,/^endef/d; /zfs/d' -i debian/rules.d/2-binary-arch.mk
/bin/sed '/ifeq ($(do_zfs),false)/,/endif/d' -i debian/rules
/bin/sed  '/zfs/d' -i debian.master/rules.d/* debian.master/d-i/modules/fs-core-modules debian.master/control.d/generic.inclusion-list debian.master/abi/*/*/*.modules debian/rules debian.master/control.d/vars.*

# Remove VBox
rm ubuntu/vbox* -rf
sed /vbox/d -i debian.master/control.d/generic.inclusion-list ubuntu/Makefile 
sed '/vbox/d' -i debian.master/reconstruct

# Compile with less modules and avoid abi check
echo 'skipmodule = true' >> debian.master/rules.d/0-common-vars.mk
echo 'skipabi = true' >> debian.master/rules.d/0-common-vars.mk
echo 'skipmodule = true' >> debian/rules.d/0-common-vars.mk
echo 'skipabi = true' >> debian/rules.d/0-common-vars.mk

# Do not label packages as unsigned
sed '/bin_pkg_name_unsigned/s/linux-image-unsigned/linux-image/' -i debian/rules.d/0-common-vars.mk
sed 's/.unsigned//' -i debian/scripts/control-create

line=$(grep -n ')-Ubuntu' debian/rules.d/0-common-vars.mk|cut -d: -f1)
sed $(expr $line - 1 ),$(expr $line + 1 )d debian/rules.d/0-common-vars.mk -i
sed s/family=ubuntu/family=trisquel/ -i debian/rules.d/0-common-vars.mk

rename s/ubuntu/trisquel/ debian.*/config/config.common.ubuntu 

find debian* -type f -name *control* -exec sed 's/ with Ubuntu patches//; s/Linux/Linux-libre/g' -i {} \;

# Descriptions should not change based on the build arch
sed 's/on DESC//; s/PKGVER on/PKGVER/; /^ DESC.$/d;' debian*/control.d/flavour-control.stub -i

sed '/^firmware/d' ./debian*/abi/*/fwinfo -i
echo > ./debian.master/d-i/firmware/nic-modules
echo > ./debian.master/d-i/firmware/scsi-modules

# Disable using udev as a fallback for firmware loading
replace "CONFIG_FW_LOADER_USER_HELPER=y" "CONFIG_FW_LOADER_USER_HELPER=n" debian.master/config

cp debian.master/config debian.hwe -a

changelog "Removed non-free bits"

cp debian/changelog debian.master/changelog
cp debian/changelog debian.hwe/changelog

compile
